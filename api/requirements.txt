fastapi[all]>=0.78.0
python-jose[cryptography]>=3.3.0,<4.0.0
passlib[bcrypt]>=1.7.4,<2.0.0
uvicorn[standard]>=0.17.6,<0.18.0
pytest
psycopg[binary,pool]==3.1.9
jwtdown-fastapi>=0.2.0
