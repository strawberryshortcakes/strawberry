steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            first_name VARCHAR(100) NOT NULL,
            last_name VARCHAR(100) NOT NULL,
            pronouns VARCHAR(100),
            email VARCHAR(50) NOT NULL UNIQUE,
            hashed_password VARCHAR(200) NOT NULL,
            username VARCHAR(100) NOT NULL UNIQUE,
            type INT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE post_types (
            id SERIAL PRIMARY KEY,
            type_name VARCHAR(50) NOT NULL UNIQUE
        );
        INSERT INTO post_types (type_name) VALUES ('Question'), ('Experience');
        """,
        # "Down" SQL statement
        """
        DROP TABLE post_types;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE author_tags (
            id SERIAL PRIMARY KEY NOT NULL,
            author_tag VARCHAR(100) UNIQUE NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE author_tags;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE genders (
            id SERIAL PRIMARY KEY NOT NULL,
            type VARCHAR(100) NOT NULL
        );
        INSERT INTO genders (type)
        VALUES
        ('Woman'), ('Man'), ('Non-binary'), ('Other');
        """,
        """
        DROP TABLE genders;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE doctors (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(150) NOT NULL,
            gender_id INT REFERENCES genders(id),
            listening INT NOT NULL,
            communication INT NOT NULL,
            response_time INT NOT NULL,
            compassion INT NOT NULL,
            respectful INT NOT NULL,
            knowledge INT NOT NULL,
            specialty VARCHAR(100) NOT NULL,
            practice_name VARCHAR(150),
            phone VARCHAR(25),
            address VARCHAR(150),
            city VARCHAR(100) NOT NULL,
            state VARCHAR(100) NOT NULL,
            zip INT
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE doctors;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE topics (
            id SERIAL PRIMARY KEY NOT NULL,
            topic_name VARCHAR(100) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE topics;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE videos (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(300) NOT NULL,
            url VARCHAR(500) NOT NULL,
            topic INT REFERENCES topics(id) ON DELETE NO ACTION
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE videos;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE websites (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(100) NOT NULL,
            url TEXT NOT NULL,
            topic INT REFERENCES topics(id) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE websites;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE posts (
            id SERIAL PRIMARY KEY NOT NULL,
            title VARCHAR(200) NOT NULL,
            author INT REFERENCES users(id) ON DELETE NO ACTION,
            content TEXT NOT NULL,
            type_id INT REFERENCES post_types(id) ON DELETE NO ACTION,
            date_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE posts;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE comments (
            id SERIAL PRIMARY KEY NOT NULL,
            author INT REFERENCES users(id),
            post_id INT REFERENCES posts(id) NOT NULL,
            content TEXT NOT NULL,
            date_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE comments;
        """
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE post_author_tags (
            post_id INT REFERENCES posts(id) ON DELETE CASCADE,
            author_tag_id INT REFERENCES author_tags(id) ON DELETE CASCADE,
            PRIMARY KEY (post_id, author_tag_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE post_author_tags;
        """
    ],
        [
        # "Up" SQL statement
        """
        CREATE TABLE post_topics (
            post_id INT REFERENCES posts(id) ON DELETE CASCADE,
            topic_id INT REFERENCES topics(id) ON DELETE CASCADE,
            PRIMARY KEY (post_id, topic_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE post_topics;
        """
    ],

]
