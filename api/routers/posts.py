from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.posts import (
    Error,
    PostIn,
    PostOut,
    PostRepository,
)

from authenticator import authenticator

router = APIRouter()


@router.post(
    "/api/posts", tags=["Posts"], response_model=Union[PostOut, Error]
)
async def create_post(
    post: PostIn,
    response: Response,
    repo: PostRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create posts.")
    result = repo.create(post, account["id"])
    if result is None:
        response.status_code = 400
        return Error(message="Unable to create new post.")
    else:
        return result



@router.get(
    "/api/posts",
    tags=["Posts"],
    response_model=Union[List[PostOut], Error],
)
async def get_all_posts(
    response: Response,
    repo: PostRepository = Depends(),
):
    result = repo.get_all_posts()
    if result is None:
        response.status_code = 400
        result = Error(message="Unable to get all posts")
    return result


@router.put(
    "/api/posts/{post_id}",
    tags=["Posts"],
    response_model=Union[PostOut, Error],
)
async def update_post(
    post_id: int,
    post: PostIn,
    response: Response,
    repo: PostRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[PostOut, Error]:

    if account is None:
        response.status_code = 401
        return Error(message="You need to be signed in to update posts.")

    existing_post = repo.get_a_post(post_id)
    if existing_post is None:
        response.status_code = 404
        return Error(message="Post not found.")

    if existing_post.author != account["id"]:
        response.status_code = 403
        return Error(message="You do not have permission to update this post.")

    result = repo.update_post(post_id, post, account["id"])
    if "message" in result:
        response.status_code = 400
        return Error(message=result["message"])
    else:
        response.status_code = 200
        return result



@router.delete(
    "/api/posts/{post_id}/", tags=["Posts"], response_model=Union[bool, Error]
)
async def delete_a_post(
    post_id: int,
    response: Response,
    repo: PostRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="You need to be signed in to delete posts.")

    existing_post = repo.get_a_post(post_id)
    if existing_post is None:
        response.status_code = 404
        return Error(message="Post not found.")
    if existing_post.author != account["id"]:
        response.status_code = 403
        return Error(message="You do not have permission to delete this post.")

    result = repo.delete_a_post(post_id, account["id"])
    if result is False:
        response.status_code = 400
        return Error(message="Unable to process delete request")
    else:
        return True



@router.get(
    "/api/posts/{post_id}",
    tags=["Posts"],
)
async def get_a_post(
    post_id: int,
    post_repo: PostRepository = Depends(),
) -> PostOut:
    post = post_repo.get_a_post(post_id)
    return post
