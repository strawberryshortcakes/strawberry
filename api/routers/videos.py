from fastapi import APIRouter, Depends, Response, status, HTTPException
from typing import List
from queries.videos import (
    VideoRepo,
    VideoIn,
    VideoOut,
    Error,
)

router = APIRouter(tags=["Videos"])


@router.post("/api/videos", response_model=VideoOut)
async def create_video(
    info: VideoIn,
    repo: VideoRepo = Depends(),
):
    try:
        video = repo.create(info)
        return video
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create video with provided information",
        )


@router.get("/api/videos/{video_id}", response_model=VideoOut)
def get_video(
    video_id: int,
    response: Response,
    queries: VideoRepo = Depends(),
):
    record = queries.get_video(video_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.get("/api/videos", response_model=List[VideoOut])
def get_all_videos(
    response: Response,
    queries: VideoRepo = Depends(),
):
    record = queries.get_all_videos()
    if record is None:
        response.status_code = 404
    else:
        return record
