from fastapi import APIRouter, Depends, Response, status, HTTPException
from typing import List, Union
from queries.websites import (
    WebsiteRepo,
    WebsiteIn,
    WebsiteOut,
    Error,
)

router = APIRouter(tags=["Websites"])


@router.post("/api/website", response_model=Union[WebsiteOut, Error])
async def create_website(
    info: WebsiteIn,
    repo: WebsiteRepo = Depends(),
):
    try:
        website = repo.create(info)
        return website
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create website",
        )


@router.get("/api/websites/{website_id}")
async def get_website(
    website_id: int,
    repo: WebsiteRepo = Depends(),
) -> WebsiteOut:
    try:
        website = repo.get_one(website_id)
        return website
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot fetch website",
        )


@router.get("/api/websites", response_model=Union[List[WebsiteOut], Error])
async def get_all_websites(
    response: Response,
    repo: WebsiteRepo = Depends(),
):
    result = repo.get_all()
    if result is None:
        response.status_code = 400
        result = Error(message="Cannot get all websites")
    else:
        return result
