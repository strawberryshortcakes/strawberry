from fastapi import APIRouter, Depends, Response, HTTPException, status
from typing import List, Union
from queries.topics import (
    TopicIn,
    TopicOut,
    TopicRepo,
    Error,
)

router = APIRouter(tags=["Topics"])


@router.get(
    "/topics",
    response_model=Union[List[TopicOut], Error],
)
def get_all_topics(repo: TopicRepo = Depends()):
    return repo.get_all()


@router.get(
    "/topics/{topic_id}",
    response_model=Union[TopicOut, Error, None],
)
def get_one_topic(
    topic_id: int,
    response: Response,
    repo: TopicRepo = Depends(),
) -> TopicOut:
    topic = repo.get_one(topic_id)
    if topic is None:
        response.status_code = 404
    return topic


@router.post(
    "/topics",
    response_model=Union[TopicOut, Error, None],
)
def create_topic(
    topic: TopicIn,
    response: Response,
    repo: TopicRepo = Depends(),
):
    try:
        new_topic = repo.create(topic)
        return new_topic
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create topic with provided information.",
        )
