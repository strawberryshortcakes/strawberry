from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from pydantic import BaseModel
from typing import List, Union
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from queries.users import (
    Error,
    UserIn,
    UserOut,
    UserRepo,
    DuplicateUserError,
)


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str


router = APIRouter(tags=["Users"])


@router.post(
    "/users/",
    response_model=UserToken | HttpError,
)
async def create_a_user(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UserRepo = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        user = UserOut(
            **repo.create(info, hashed_password=hashed_password).dict()
        )
    except DuplicateUserError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a user with those credentials.",
        )
    form = UserForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    return UserToken(user=user, **token.dict())


@router.get("/token", tags=["Authentication"], response_model=UserToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> UserToken | None:
    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }


@router.get("/users/", response_model=Union[List[UserOut], Error])
async def get_all_users(
    response: Response,
    repo: UserRepo = Depends(),
    user: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[UserOut, Error]:
    if user is None:
        response.status_code = 401
        return Error(message="Sign in to get users.")
    result = repo.get_all()
    if result is None:
        response.status_code = 404
        result = Error(message="No users exist.")
    return result


@router.get("/users/{user_id}")
async def get_one_user(
    user_id: int,
    response: Response,
    users: UserRepo = Depends(),
    user: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[UserOut, Error]:
    if user is None:
        response.status_code = 401
        return Error(message="Sign in to get a specific user.")
    result = users.get_one(user_id)
    if result is None:
        response.status_code = 404
        result = Error(message=f"A user with the id {user_id} was not found.")
    return result
