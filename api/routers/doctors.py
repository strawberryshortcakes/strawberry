from fastapi import APIRouter, Depends, Response, status, HTTPException
from typing import List, Union
from queries.doctors import (
    DoctorRepo,
    DoctorIn,
    DoctorOut,
    Error,
)

router = APIRouter(tags=["Doctors"])


@router.post("/api/doctors", response_model=Union[DoctorOut, Error])
async def create_doctor(
    info: DoctorIn,
    repo: DoctorRepo = Depends(),
):
    try:
        doctor = repo.create(info)
        return doctor
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create doctor with provided information",
        )


@router.get("/api/doctors", response_model=Union[List[DoctorOut], Error])
async def get_all_doctors(
    response: Response,
    repo: DoctorRepo = Depends(),
):
    result = repo.get_all()
    if result is None:
        response.status_code = 400
        result = Error(message="Cannot get list of doctors")
    else:
        return result


@router.get("/api/doctors/{doctor_id}")
async def get_doctor(
    doctor_id: int,
    repo: DoctorRepo = Depends(),
) -> DoctorOut:
    try:
        doctor = repo.get_one(doctor_id)
        return doctor
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot fetch doctor",
        )


@router.delete("/api/doctors/{doctor_id}/", response_model=Union[bool, Error])
async def delete_doctor(
    doctor_id: int,
    response: Response,
    repo: DoctorRepo = Depends(),
):
    result = repo.delete(doctor_id)

    if result is False:
        response.status_code = 400
        result = Error(message="unable to process delete request")

    return result
