from fastapi import APIRouter, Depends, Response, status, HTTPException
from typing import List
from queries.author_tags import (
    AuthorTagOut,
    AuthorTagRepository,
    AuthorTagIn,
    Error,
)

router = APIRouter(tags=["Author Tags"])


async def get_author_tag_repo():
    return AuthorTagRepository()

@router.post("/api/author_tags", response_model=AuthorTagOut)
async def get_or_create_author_tag(
    info: AuthorTagIn,
    repo: AuthorTagRepository = Depends(get_author_tag_repo),
):
    try:
        tag_id = repo.get_or_create_author_tag(info.author_tag)
        return AuthorTagOut(id=tag_id, author_tag=info.author_tag)
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create author tag.",
        )


@router.get("/api/author_tags/{author_tag_id}", response_model=AuthorTagOut)
async def get_one_author_tag(
    author_tag_id: int,
    repo: AuthorTagRepository = Depends(get_author_tag_repo),
):
    tag = repo.get_one_author_tag(author_tag_id)
    if not tag:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Author tag not found.",
        )
    return tag


@router.get("/api/author_tags", response_model=List[AuthorTagOut])
async def get_all_author_tags(
    repo: AuthorTagRepository = Depends(get_author_tag_repo),
):
    try:
        return repo.get_all_author_tags()
    except Error:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Could not fetch all author tags.",
        )
