from fastapi import APIRouter, Depends, Response, status, HTTPException
from typing import List
from queries.genders import (
    GenderRepo,
    GenderIn,
    GenderOut,
    Error,
)

router = APIRouter(tags=["Genders"])


@router.post("/api/genders", response_model=GenderOut)
async def create_gender(
    info: GenderIn,
    repo: GenderRepo = Depends(),
):
    try:
        gender = repo.create(info)
        return gender
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create gender with provided information",
        )


@router.get("/api/genders/{gender_id}", response_model=GenderOut)
def get_gender(
    gender_id: int,
    response: Response,
    queries: GenderRepo = Depends(),
):
    record = queries.get_gender(gender_id)
    if record is None:
        response.status_code = 404
    else:
        return record


@router.get("/api/genders", response_model=List[GenderOut])
def get_all_genders(
    response: Response,
    queries: GenderRepo = Depends(),
):
    record = queries.get_all_genders()
    if record is None:
        response.status_code = 404
    else:
        return record
