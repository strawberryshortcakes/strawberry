from fastapi import APIRouter, Depends, Response, status, HTTPException
from typing import List, Union
from queries.comments import (
    CommentRepo,
    CommentIn,
    CommentOut,
    Error,
)

router = APIRouter(tags=["Comments"])


@router.post("/api/comment", response_model=Union[CommentOut, Error])
async def create_comment(
    comment: CommentIn,
    response: Response,
    repo: CommentRepo = Depends(),
):
    result = repo.create(comment)
    if result is None:
        response.status_code = 400
        return Error(message="Cannot create comment")
    else:
        return result


@router.get("/api/comments/{comment_id}")
async def get_comment(
    comment_id: int,
    repo: CommentRepo = Depends(),
) -> CommentOut:
    try:
        comment = repo.get_one(comment_id)
        return comment
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot get comment",
        )


@router.get("/api/comments", response_model=Union[List[CommentOut], Error])
async def get_all_comments(
    response: Response,
    repo: CommentRepo = Depends(),
):
    try:
        comments = repo.get_all()
        return comments
    except Error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot get all comments",
        )
