import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.users import UserRepo, UserOutWithPassword


class SecureAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        users: UserRepo,
    ):
        # Use your repo to get the account based on the
        # username (which could be an email)
        return users.get(username)

    def get_account_getter(
        self,
        users: UserRepo = Depends(),
    ):
        # Return the accounts. That's it.
        return users

    def get_hashed_password(self, user: UserOutWithPassword):
        # Return the encrypted password value from your
        # account object
        return user.hashed_password


authenticator = SecureAuthenticator(os.environ["SIGNING_KEY"])
