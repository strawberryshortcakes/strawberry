from pydantic import BaseModel
from typing import Optional, List, Union
from datetime import datetime
from queries.pool import pool
from queries.posts import PostRepository


class CommentIn(BaseModel):
    author: int
    post_id: int
    content: str
    date_created: datetime


class CommentOut(BaseModel):
    id: int
    author: int
    post_id: int
    content: str
    date_created: datetime


class Error(BaseModel):
    message: str


class CommentRepo:
    post_repo = PostRepository()

    def comment_in_to_out(self, id: int, comment: CommentIn):
        old_data = comment.dict()
        return CommentOut(id=id, **old_data)

    def record_to_comment_out(self, record):
        return CommentOut(
            id=record[0],
            author=record[1],
            post_id=record[2],
            content=record[3],
            date_created=record[4],
        )

    def create(self, comment: CommentIn) -> Union[CommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    post_id = self.post_repo.get_a_post(comment.post_id)
                    db.execute(
                        """
                        INSERT INTO comments
                            (author,
                            post_id,
                            content,
                            date_created)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            comment.author,
                            comment.post_id,
                            comment.content,
                            comment.date_created,
                        ],
                    )

                    tuple = db.fetchone()
                    id = tuple[0]
                    old_data = comment.dict()

                    return CommentOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not create comment."}

    def get_one(self, comment_id: int) -> Optional[CommentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, author, post_id, content, date_created
                        FROM comments
                        WHERE id = %s
                        """,
                        [comment_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_comment_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get comment."}

    def get_all(self) -> Union[List[CommentOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, author, post_id, content, date_created
                        FROM comments
                        """
                    )
                    return [
                        self.record_to_comment_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get comments."}
