from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class TopicIn(BaseModel):
    topic_name: str


class TopicOut(BaseModel):
    id: int
    topic_name: str


class Error(BaseModel):
    message: str


class TopicRepo:
    # def topic_in_to_out(self, id: int, topic: TopicIn):
    #     old_data = topic.dict()
    #     return TopicOut(id=id, **old_data)

    def record_to_topic_out(self, record):
        return TopicOut(
            id=record[0],
            topic_name=record[1],
        )

    def create(self, topic: TopicIn) -> Union[TopicOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO topics
                            (topic_name
                            )
                        VALUES
                            (%s)
                        RETURNING id;
                        """,
                        [
                            topic.topic_name,
                        ],
                    )

                    record = db.fetchone()
                    id = record[0]
                    old_data = topic.dict()

                    return TopicOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return Error(message="Could not create a topic.")

    def get_one(self, topic_id: int) -> Union[TopicOut, Error, None]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, topic_name
                        FROM topics
                        WHERE id = %s;
                        """,
                        [topic_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_topic_out(record)
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve that topic.")

    def get_all(self) -> Union[List[TopicOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            topic_name
                        FROM topics
                        ORDER BY topic_name;
                        """
                    )
                    return [
                        self.record_to_topic_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve all topics.")
