from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class AuthorTagIn(BaseModel):
    author_tag: str


class AuthorTagOut(BaseModel):
    id: int
    author_tag: str


class Error(BaseModel):
    message: str


class AuthorTagRepository:
    def author_tag_in_to_out(self, id: int, author_tag: AuthorTagIn):
        old_data = author_tag.dict()
        return AuthorTagOut(id=id, **old_data)

    def record_to_author_tag_out(self, record):
        return AuthorTagOut(
            id=record[0],
            author_tag=record[1],
        )
    @staticmethod
    def get_or_create_author_tag(tag: str) -> int:
        with pool.connection() as conn:
            with conn.cursor() as db:

                db.execute("SELECT id FROM author_tags WHERE author_tag = %s", [tag])
                tag_id = db.fetchone()

                if not tag_id:
                    db.execute("INSERT INTO author_tags (author_tag) VALUES (%s) RETURNING id;", [tag])
                    tag_id = db.fetchone()[0]
                else:
                    tag_id = tag_id[0]

                return tag_id


    def get_one_author_tag(self, author_tag_id: int) -> Optional[AuthorTagOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, author_tag
                        FROM author_tags
                        WHERE id = %s
                        """,
                        [author_tag_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_author_tag_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get author tag."}

    def get_all_author_tags(self) -> Union[List[AuthorTagOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, author_tag
                        FROM author_tags
                        """
                    )
                    return [
                        self.record_to_author_tag_out(record)
                        for record in result.fetchall()
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all author tags"}
