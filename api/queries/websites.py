from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class WebsiteIn(BaseModel):
    title: str
    url: str
    topic: int


class WebsiteOut(BaseModel):
    id: int
    title: str
    url: str
    topic: int


class Error(BaseModel):
    message: str


class WebsiteRepo:
    def website_in_to_out(self, id: int, website: WebsiteIn):
        old_data = website.dict()
        return WebsiteOut(id=id, **old_data)

    def record_to_website_out(self, record):
        return WebsiteOut(
            id=record[0],
            title=record[1],
            url=record[2],
            topic=record[3],
        )

    def create(self, website: WebsiteIn) -> WebsiteOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO websites
                            (title,
                            url,
                            topic)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [website.title, website.url, website.topic],
                    )

                    tuple = db.fetchone()
                    id = tuple[0]
                    old_data = website.dict()

                    return WebsiteOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not add website."}

    def get_one(self, website_id: int) -> Optional[WebsiteOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, url, topic
                        FROM websites
                        WHERE id = %s
                        """,
                        [website_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_website_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get website."}

    def get_all(self) -> Union[List[WebsiteOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, title, url, topic
                        FROM websites
                        """
                    )
                    return [
                        self.record_to_website_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get websites."}
