from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class GenderIn(BaseModel):
    type: str


class GenderOut(BaseModel):
    id: int
    type: str


class Error(BaseModel):
    message: str


class GenderRepo:
    def gender_in_to_out(self, id: int, gender: GenderIn):
        old_data = gender.dict()
        return GenderOut(id=id, **old_data)

    def record_to_gender_out(self, record):
        return GenderOut(
            id=record[0],
            type=record[1],
        )

    def create(self, gender: GenderIn) -> GenderOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO genders
                            (type)
                        VALUES
                            (%s)
                        RETURNING id;
                        """,
                        [gender.type],
                    )

                    tuple = db.fetchone()
                    id = tuple[0]
                    old_data = gender.dict()

                    return GenderOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not create gender."}

    def get_gender(self, gender_id: int) -> Optional[GenderOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, type
                        FROM genders
                        WHERE id = %s
                        """,
                        [gender_id],
                    )

                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_gender_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get gender."}

    def get_all_genders(self) -> Union[List[GenderOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, type
                        FROM genders
                        """
                    )
                    return [
                        self.record_to_gender_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all genders."}