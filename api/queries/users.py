from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class UserIn(BaseModel):
    first_name: str
    last_name: str
    pronouns: str | None = None
    email: str
    password: str
    username: str
    type: int | None = None


class UserOut(BaseModel):
    id: int
    first_name: str
    last_name: str
    pronouns: str | None = None
    email: str
    username: str
    type: int | None = None


class UserOutWithPassword(UserOut):
    hashed_password: str


class DuplicateUserError(ValueError):
    pass


class Error(BaseModel):
    message: str


class UserRepo:
    def record_to_user_out(self, record) -> UserOutWithPassword:
        return UserOutWithPassword(
            id=record[0],
            first_name=record[1],
            last_name=record[2],
            pronouns=record[3],
            email=record[4],
            hashed_password=record[5],
            username=record[6],
            type=record[7],
        )

    # def user_in_to_out(self, id: int, user: UserIn):
    #     old_data = user.dict()
    #     return UserOut(id=id, **old_data)

    def create(
        self, user: UserIn, hashed_password: str
    ) -> Union[UserOutWithPassword, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO users
                            (first_name,
                            last_name,
                            pronouns,
                            email,
                            hashed_password,
                            username,
                            type
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING
                        id,
                        first_name,
                        last_name,
                        pronouns,
                        email,
                        hashed_password,
                        username,
                        type;
                        """,
                        [
                            user.first_name,
                            user.last_name,
                            user.pronouns,
                            user.email,
                            hashed_password,
                            user.username,
                            user.type,
                        ],
                    )

                    record = db.fetchone()
                    id = record[0]
                    result = UserOutWithPassword(
                        id=id,
                        first_name=user.first_name,
                        last_name=user.last_name,
                        pronouns=user.pronouns,
                        email=user.email,
                        hashed_password=hashed_password,
                        username=user.username,
                        type=user.type,
                    )
                    print(result)
                    return result

        except Exception as e:
            print(e)
            return Error(message="Could not create a user.")

    def get_one(self, user_id: int) -> Optional[UserOutWithPassword]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        first_name,
                        last_name,
                        pronouns,
                        email,
                        hashed_password,
                        username,
                        type
                        FROM users
                        WHERE id = %s
                        """,
                        [user_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_user_out(record)
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve that user.")

    def get_all(self) -> Union[List[UserOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        first_name,
                        last_name,
                        pronouns,
                        email,
                        hashed_password,
                        username,
                        type
                        FROM users
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_user_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return Error(message="Could not retrieve all users.")

    def get(self, username: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        id,
                        first_name,
                        last_name,
                        pronouns,
                        email,
                        hashed_password,
                        username,
                        type
                        FROM users
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        print("UserRepo.get did not produce result.")
                        return None
                    return self.record_to_user_out(record)
        except Exception:
            return Error(message="Could not retrieve user.")
