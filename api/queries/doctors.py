from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class DoctorIn(BaseModel):
    name: str
    gender_id: int
    listening: int
    communication: int
    response_time: int
    compassion: int
    respectful: int
    knowledge: int
    specialty: str
    practice_name: str
    phone: str
    address: str
    city: str
    state: str
    zip: int


class DoctorOut(BaseModel):
    id: int
    name: str
    gender_id: int
    listening: int
    communication: int
    response_time: int
    compassion: int
    respectful: int
    knowledge: int
    specialty: str
    practice_name: str
    phone: str
    address: str
    city: str
    state: str
    zip: int


class Error(BaseModel):
    message: str


class DoctorRepo:
    def doctor_in_to_out(self, id: int, doctor: DoctorIn):
        old_data = doctor.dict()
        return DoctorOut(id=id, **old_data)

    def record_to_doctor_out(self, record):
        return DoctorOut(
            id=record[0],
            name=record[1],
            gender_id=record[2],
            listening=record[3],
            communication=record[4],
            response_time=record[5],
            compassion=record[6],
            respectful=record[7],
            knowledge=record[8],
            specialty=record[9],
            practice_name=record[10],
            phone=record[11],
            address=record[12],
            city=record[13],
            state=record[14],
            zip=record[15],
        )

    def create(self, doctor: DoctorIn) -> DoctorOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO doctors
                            (name,
                            gender_id,
                            listening,
                            communication,
                            response_time,
                            compassion,
                            respectful,
                            knowledge,
                            specialty,
                            practice_name,
                            phone,
                            address,
                            city,
                            state,
                            zip)
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            doctor.name,
                            doctor.gender_id,
                            doctor.listening,
                            doctor.communication,
                            doctor.response_time,
                            doctor.compassion,
                            doctor.respectful,
                            doctor.knowledge,
                            doctor.specialty,
                            doctor.practice_name,
                            doctor.phone,
                            doctor.address,
                            doctor.city,
                            doctor.state,
                            doctor.zip,
                        ],
                    )

                    tuple = db.fetchone()
                    id = tuple[0]
                    old_data = doctor.dict()

                    return DoctorOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not create a doctor."}

    def get_one(self, doctor_id: int) -> Optional[DoctorOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            name,
                            gender_id,
                            listening,
                            communication,
                            response_time,
                            compassion,
                            respectful,
                            knowledge,
                            specialty,
                            practice_name,
                            phone,
                            address,
                            city,
                            state,
                            zip
                        FROM doctors
                        WHERE id = %s
                        """,
                        [doctor_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_doctor_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get doctor."}

    def get_all(self) -> Union[List[DoctorOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            name,
                            gender_id,
                            listening,
                            communication,
                            response_time,
                            compassion,
                            respectful,
                            knowledge,
                            specialty,
                            practice_name,
                            phone,
                            address,
                            city,
                            state,
                            zip
                        FROM doctors
                        """
                    )
                    return [
                        self.record_to_doctor_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all doctors"}

    def delete(self, doctor_id) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM doctors
                        WHERE id = %s
                        """,
                        [doctor_id],
                    )
                    return True
        except Exception:
            return {"message": "Could not delete doctor."}
