from pydantic import BaseModel
from datetime import datetime
from queries.pool import pool
from typing import List, Optional, Union
from queries.author_tags import AuthorTagRepository



class Error(BaseModel):
    message: str



class PostIn(BaseModel):
    title: str
    author_tags: List[str]
    topic: List[int]
    content: str
    type_id: int
    date_created: Optional[datetime]


class PostOut(BaseModel):
    id: int
    title: str
    author: int
    author_tags: List[str]
    topic: List[int]
    content: str
    type_id: int
    date_created: datetime


class PostRepository:

    author_tag_repo = AuthorTagRepository()

    @staticmethod
    def fetch_author_tags_for_post(db, post_id: int) -> List[str]:
        db.execute(
            """
            SELECT at.author_tag
            FROM post_author_tags pat
            JOIN author_tags at ON pat.author_tag_id = at.id
            WHERE pat.post_id = %s;
            """,
            [post_id]
        )
        return [record[0] for record in db.fetchall()]

    def post_in_to_out(self, id: int, post: PostIn):
        old_data = post.dict()
        return PostOut(id=id, **old_data)

    def record_to_post_out(self, db, record):
        post_id, title, author, content, type_id, date_created = record
        author_tags = self.fetch_author_tags_for_post(db, post_id)
        topics = self.fetch_topics_for_post(db, post_id)
        return PostOut(
            id=post_id,
            title=title,
            author=author,
            content=content,
            type_id=type_id,
            date_created=date_created,
            author_tags=author_tags,
            topic=topics
        )

    def fetch_topics_for_post(self, db, post_id: int) -> List[int]:
        db.execute(
            """
            SELECT topic_id
            FROM post_topics
            WHERE post_id = %s;
            """,
            [post_id]
        )
        return [record[0] for record in db.fetchall()]

    def get_a_post(self, post_id) -> Optional[PostOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                    """
                        SELECT
                            id,
                            title,
                            author,
                            content,
                            type_id,
                            date_created
                        FROM posts WHERE id = %s;
                    """, [post_id])
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_post_out(db, record)

        except Exception as e:
            print(e)
            return {"message": "Could not get that post."}

    def delete_a_post(self, post_id, current_author_id) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM posts
                        WHERE id = %s AND author = %s
                        """,
                        [post_id, current_author_id],
                    )
                    return db.rowcount > 0
        except Exception:
            return False

    def update_post(self, post_id: int, post: PostIn, current_author_id: int) -> Union[PostOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:

                    db.execute(
                        """
                        SELECT author FROM posts WHERE id = %s
                        """,
                        [post_id]
                    )
                    record = db.fetchone()
                    if not record:
                        return Error(message="No post found with the provided ID.")

                    if record[0] != current_author_id:
                        return Error(message="You do not have permission to update this post.")


                    db.execute(
                        """
                        UPDATE posts
                        SET title = %s,
                            content = %s,
                            type_id = %s,
                            date_created = %s
                        WHERE id = %s
                        """,
                        [
                            post.title,
                            post.content,
                            post.type_id,
                            post.date_created,
                            post_id,
                        ],
                    )

                    db.execute("DELETE FROM post_author_tags WHERE post_id = %s", [post_id])
                    for tag in post.author_tags:
                        author_tag_id = self.author_tag_repo.get_or_create_author_tag(tag)
                        db.execute(
                            """
                            INSERT INTO post_author_tags
                                (post_id, author_tag_id)
                            VALUES
                                (%s, %s)
                            """,
                            [post_id, author_tag_id]
                        )

                    if db.rowcount == 0:
                        return Error(message="Failed to update the post.")

                with conn.cursor() as db_post:
                    db_post.execute(
                        """
                        SELECT
                            id,
                            title,
                            author,
                            content,
                            type_id,
                            date_created
                        FROM posts
                        WHERE id = %s
                        """,
                        [post_id],
                    )
                    record = db_post.fetchone()

                    if record is None:
                        return Error(message="Failed to retrieve the updated post.")

                    return self.record_to_post_out(db_post, record)

        except Exception as e:
            return Error(message=f"Could not update post: {e}")



    def get_all_posts(self) -> Union[List[PostOut], Error]:
        try:
            posts = []
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            title,
                            author,
                            content,
                            type_id,
                            date_created
                        FROM posts
                        ORDER BY id
                        """
                    )
                    records = list(db)
                    print(records)
                    for record in records:
                        post_out = self.record_to_post_out(db, record)
                        posts.append(post_out)
            return posts
        except Exception as e:
            print(e)
            return Error(message="Could not get all posts")


    def create(self, post: PostIn, author_id: int) -> Union[PostOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO posts
                            (title, author, content, type_id, date_created)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            post.title,
                            author_id,
                            post.content,
                            post.type_id,
                            post.date_created or datetime.now(),
                        ],
                    )
                    post_id = db.fetchone()[0]

                    if post_id is None:
                        return Error(message="Failed to create post.")

                    for tag in post.author_tags:
                        author_tag_id = self.author_tag_repo.get_or_create_author_tag(tag)
                        db.execute(
                            """
                            INSERT INTO post_author_tags
                                (post_id, author_tag_id)
                            VALUES
                                (%s, %s)
                            """,
                            [post_id, author_tag_id]
                        )
                    for topic in post.topic:
                        db.execute(
                        """
                        INSERT INTO post_topics
                            (post_id, topic_id)
                        VALUES
                            (%s, %s)
                        """,
                        [post_id, topic]
                        )

                    conn.commit()
                    db.execute("SELECT * FROM posts WHERE id = %s;", [post_id])
                    record = db.fetchone()
                    if not record:
                        return Error(message="Failed to retrieve the newly created post.")
                    return self.record_to_post_out(db, record)


        except Exception as e:
            print(f"Error: {e}")
            return Error(message=str(e))
