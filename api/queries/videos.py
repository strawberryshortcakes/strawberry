from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class VideoIn(BaseModel):
    title: str
    url: str
    topic: int


class VideoOut(BaseModel):
    id: int
    title: str
    url: str
    topic: int


class Error(BaseModel):
    message: str


class VideoRepo:
    def video_in_to_out(self, id: int, video: VideoIn):
        old_data = video.dict()
        return VideoOut(id=id, **old_data)

    def record_to_video_out(self, record):
        return VideoOut(
            id=record[0],
            title=record[1],
            url=record[2],
            topic=record[3],
        )

    def create(self, video: VideoIn) -> VideoOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        INSERT INTO videos
                            (title,
                            url,
                            topic)
                        VALUES
                            (%s,%s,%s)
                        RETURNING id;
                        """,
                        [video.title,
                         video.url,
                         video.topic],
                    )

                    tuple = db.fetchone()
                    id = tuple[0]
                    old_data = video.dict()

                    return VideoOut(id=id, **old_data)
        except Exception as e:
            print(e)
            return {"message": "Could not add video."}

    def get_video(self, video_id: int) -> Optional[VideoOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            title,
                            url,
                            topic
                        FROM videos
                        WHERE id = %s
                        """,
                        [video_id],
                    )

                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_video_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get video."}

    def get_all_videos(self) -> Union[List[VideoOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                            title,
                            url,
                            topic
                        FROM videos
                        """
                    )
                    return [
                        self.record_to_video_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all videos."}