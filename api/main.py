from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import (
    doctors,
    comments,
    websites,
    topics,
    users,
    posts,
    author_tags,
    videos,
    genders,
)
import os

app = FastAPI()

tags_metadata = [
    {"name": "Doctors", "description": "Manage Doctors"},
    {
        "name": "Posts",
        "description": "Manage Posts",
    },
    {
        "name": "Author Tags",
        "description": "Manage Author Tags",
    },
    {
        "name": "Topics",
        "description": "Manage Topics",
    },
    {
        "name": "Users",
        "description": "Manage Users",
    },
    {
        "name": "Genders",
        "description": "Manage Genders",
    },
    {
        "name": "Videos",
        "description": "Manage Videos",
    },
    {
        "name": "Comments",
        "description": "Manage Comments",
    },
    {
        "name": "Websites",
        "description": "Manage Websites",
    },
]


app = FastAPI(
    title="Strawberry.",
    description="These are the API endpoints used by the front-end for Strawberry.",
    openapi_tags=tags_metadata,
)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:3000")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(doctors.router)
app.include_router(posts.router)
app.include_router(author_tags.router)
app.include_router(topics.router)
app.include_router(users.router)
app.include_router(videos.router)
app.include_router(genders.router)
app.include_router(comments.router)
app.include_router(websites.router)


@app.get("/")
def read_root():
    return {"Hello": "World"}
